require('ts-node').register();
const express = require('express');
const bodyParser = require('body-parser');
const pino = require('pino-http')({
    level: 'info'
}); // https://github.com/mcollina/pino#pinoopts-stream

const LAMBDA_PATH = process.env.LAMBDA_PATH;
const LAMBDA_PORT = process.env.LAMBDA_PORT || 9000;

const app = module.exports.app = exports.app = express();

app.use(bodyParser.json());
app.use(pino); // instead of winston logger

app.use('*', (req, res, next) => {
    req.logger = req.log; // so that it is available elsewhere.
    next();
});

const router = express.Router({ // eslint-disable-line new-cap
    caseSensitive: true
});
app.use(router);

const lambda = require(LAMBDA_PATH).default;
// create a route that can talk to the lambda function
router.post('/', (req, res) => {
    lambda(req.body, {}, (err, result) => {
        if (err) {
            return res.status(406).send(result);
        }

        return res.send(result);
    });
});

const listener = app.listen(LAMBDA_PORT, () => {
    console.log(`Server started on port ${listener.address().port}`);
});
