// import * as bcrypt from 'bcrypt';
import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
const key = 'MIIEpAIBAAKCAQEAl8xxOa1JKw5Ju4LN6HZsI2hkCJi3r+3aoBXpk0NHvrMZ95VS';

export class CouchMethods {
    /**
     * Inserts a new customer into Custpmers database
     * @param couch <CouchDb>
     * @param customer <customer>
     * @returns Promise
     */
    public static register(couch: any, customer: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            // encrypt user password
            (async () => {
                let hash = await bcrypt.hash(customer.password, 10);
                customer.password = hash;
                couch.viewAsync('customer-view', 'customer-index', { keys: [customer.email] }).then(([body, headers]) => {
                    if (body && body.rows.length < 1) {
                        return couch.insertAsync(customer).then((resp) => {
                            //  create a jwt token and then return it to the client
                            (async () => {
                                let customerDetails = {
                                    email: customer.email,
                                    firstName: customer.firstName,
                                    lastName: customer.lastName
                                };
                                let token = await this.createToken(customerDetails);
                                resolve({
                                    email: customer.email,
                                    firstName: customer.firstName,
                                    lastName: customer.lastName,
                                    token
                                });
                            })();
                        }).catch((error) => reject(error));
                    } else {
                        resolve({ error: 'Customer Already Exists' });
                    }
                }).catch((error) => reject(error));
            })();
            // Check that customer does not exist already
        });
    }
    /**
     * Inserts a new document into a couch database
     * @param couch <CouchDb>
     * @param document <Basket>
     * @returns Promise
     */
    public static login(couch: any, customer: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            couch.viewAsync('customer-view', 'customer-index', { keys: [customer.email] }).then(([body, headers]) => {
                (async () => {
                    if (body && body.rows.length > 0) {
                        // Check the email and password are the same as what is stored in the database
                        let passMatch = await bcrypt.compare(customer.password, body.rows[0].value.password);
                        if ((body.rows[0].value.email === customer.email) && passMatch) {
                            let customerDetails = {
                                id: body.rows[0].value._id,
                                email: customer.email,
                                firstName: body.rows[0].value.firstName,
                                lastName: body.rows[0].value.lastName,
                                shippingDetails: body.rows[0].value.shippingDetails
                            };
                            let token = await this.createToken(customerDetails);
                            resolve({
                                id: customerDetails.id,
                                email: customerDetails.email,
                                firstName: customerDetails.firstName,
                                lastName: customerDetails.lastName,
                                token,
                                shippingDetails: customerDetails.shippingDetails
                            });
                        } else {
                            resolve({ error: 'Email or Password is wrong' });
                        }
                    } else {
                        resolve({ error: 'Invalid Login Details' });
                    }
                })();
            }).catch((error) => reject(error));
        });
    }

    /**
     * Validates that a users login credentials are valid
     * @param token jwt token
     */
    public static validateToken(token: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            return jwt.verify(token, key, (err, payload) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(true);
                }
            });
        });
    }

    /**
     * Gets the customer details
     * @param couch counchdb instance
     * @param email the customer email
     */
    public static details(couch: any, email: string) {
        return new Promise<any>((resolve, reject) => {
            couch.viewAsync('customer-view', 'customer-index', { keys: [email] }).then(([body, headers]) => {
                (async () => {
                    if (body && body.rows.length > 0) {
                        let customerDet = {
                            id: body.rows[0].value._id,
                            email: body.rows[0].value.email,
                            firstName: body.rows[0].value.firstName,
                            lastName: body.rows[0].value.lastName,
                            shippingDetails: body.rows[0].value.shippingDetails
                        };
                        resolve(customerDet);
                    } else {
                        reject({ Error: 'Customer Does not exists' });
                    }
                })();
            });
        });
    }

    /**
     * Add a more shippimg details to the customer profile
     * @param couch couchdb instance
     * @param email customer email address
     * @param shippingDetails the new shippoing details to add
     */
    public static updateShippingDetails(couch: any, email: string, shippingDetails: object): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            couch.viewAsync('customer-view', 'customer-index', { keys: [email] }).then(([body, headers]) => {
                if (body && body.rows.length > 0) {
                    let customer = body.rows[0].value;

                    customer.shippingDetails = customer.shippingDetails || [];
                    customer.shippingDetails.push(shippingDetails);
                    couch.insertAsync(customer).then((resp) => {
                        if (!resp.error) {
                            resolve(true);
                        } else {
                            reject(resp);
                        }
                    }).catch((err) => reject(err));
                } else {
                    reject({ error: 'Customer Does not exists' });
                }
            }).catch((err) => reject(err));
        });
    }

    /**
     * Changes the shippingDetails stored for the customer, gotten from a delete operation
     * @param couch couchdb instance
     * @param email customer email
     * @param shippingDetails The new shipping details
     */
    public static editShippingDetails(couch: any, email: string, shippingDetails: object): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            couch.viewAsync('customer-view', 'customer-index', { keys: [email] }).then(([body, headers]) => {
                if (body && body.rows.length > 0) {
                    let customer = body.rows[0].value;

                    customer.shippingDetails = shippingDetails;
                    couch.insertAsync(customer).then((resp) => {
                        if (!resp.error) {
                            resolve(true);
                        } else {
                            reject(resp);
                        }
                    }).catch((err) => reject(err));
                } else {
                    resolve({ error: 'Customer Does not exists' });
                }
            }).catch((err) => reject(err));
        });
    }

    /**
     * Adds a default: true to the address to be set as the default address
     * and defaulkt: false to the rest that are not the default address
     * @param couch couchdb
     * @param email customer email address
     * @param index the index that the address is stored
     * @param type billing or shipping
     */
    public static changeDefaultAddress(couch: any, email: string, index: number, type: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            couch.viewAsync('customer-view', 'customer-index', { keys: [email] }).then(([body, headers]) => {
                if (body && body.rows.length > 0) {
                    let customer = body.rows[0].value;
                    let shipping = customer.shippingDetails;
                    shipping.forEach((shippingDetail, idx, shipArr) => {
                        if (type.includes('billing')) {
                            if (index === idx) {
                                shippingDetail.billingAddress.default = true;
                            } else if (type.includes('shipping')) {
                                shippingDetail.shippingAddress.default = true;
                            }
                            customer.shippingDetails.splice(index, 1, shippingDetail);
                        } else {
                            if (type.includes('billing')) {
                                shippingDetail.billingAddress.default = false;
                            } else {
                                shippingDetail.shippingAddress.default = false;
                            }
                            customer.shippingDetails.splice(index, 1, shippingDetail);
                        }
                    });

                    couch.insertAsync(customer).then((resp) => {
                        resolve(true);
                    }).catch((err) => reject(err));
                } else {
                    reject({ Error: 'Customer Does not exists' });
                }
            }).catch((err) => reject(err));
        });
    }

    /**
     * Get the last saved user basket
     * @param couch <CouchDb>
     * @param username string
     */
    public static getBasket(couch: any, userId: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            couch.viewAsync('user_baskets', 'users_docs', { key: userId, include_docs: true }).then(([body, headers]) => {
                if (body && body.rows.length) {
                    let lastBasket = body.rows.reduce((a, b) => {
                        if (a && b) {
                            return (a.value.modified > b.value.modified) ? a : b;
                        } else {
                            return a || b;
                        }
                    }, null);
                    resolve(lastBasket.value);
                } else {
                    resolve({ Error: 'Empty Body' });
                }
            }).catch((error) => reject(error));
        });
    }

    /**
     * Gets the basket/order based on the basket id
     * @param couch Couch Instance
     * @param id string
     */
    public static getBasketbyId(couch: any, id: string): Promise<any> {
        return new Promise<object>((resolve, reject) => {
            couch.viewAsync('update_order', 'update_order_view', { key: id, include_docs: true })
                .then(([body, headers]) => {
                    if (body && body.rows.length) {
                        resolve(body.rows[0].value);
                    } else {
                        resolve({ error: 'Id does not exist' });
                    }
                }).catch((err) => reject(err));
        });
    }

    /**
     * Updates Customer Database with the customer stripe id
     * @param couch CouchInstance
     * @param email string
     * @param data Object
     */
    public static updateCustomer(couch: any, email: string, data: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            couch.viewAsync('customer-view', 'customer-index', { keys: [email] }).then(([body, headers]) => {
                if (body && body.rows.length > 0) {
                    let customer = body.rows[0].value;
                    customer.stripe = (customer.stripe) ? customer.stripe : {};
                    customer.stripe.id = (data.stripe.id) ? data.stripe.id : '';

                    couch.insertAsync(customer).then((resp) => {
                        resolve(true);
                    }).catch((err) => reject(err));
                } else {
                    resolve({ error: 'Customer Does not exists' });
                }
            }).catch((err) => reject(err));
        });
    }

    /**
     * Changes the pasword of the customer
     * @param couch Couch Instance
     * @param email string
     * @param oldPassword string
     * @param newPassword string
     */
    public static changePassword(couch: any, email: string, oldPassword: string, newPassword: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            couch.viewAsync('customer-view', 'customer-index', { keys: [email] }).then(([body, headers]) => {
                if (body && body.rows.length > 0) {
                    (async () => {
                        let customer = body.rows[0].value;
                        let passMatch = await bcrypt.compare(oldPassword, customer.password);
                        if (passMatch) {
                            customer.password = await bcrypt.hash(newPassword, 10);

                            couch.insertAsync(customer).then((resp) => {
                                resolve({ success: 'success' });
                            }).catch((err) => reject(err));
                        } else {
                            resolve({ error: 'failed' });
                        }

                    })();
                } else {
                    resolve('Customer Does Not Exist');
                }
            }).catch((err) => reject(err));
        });
    }

    /**
     * Creates a JWT token
     * @param customerDetails
     */
    private static createToken(customerDetails: object): Promise<string> {
        return new Promise((resolve, reject) => {
            return jwt.sign(customerDetails, key, { expiresIn: '24h' },
                (err, payload) => {
                    if (err) {
                        reject(err);
                    }
                    resolve(payload);
                });
        });
    }
}
