import { CouchMethods } from '../helpers/couch-methods';
import { StripeMethods } from '../helpers/stripe-methods';
import { LookupMethods } from '../helpers/lookup-methods';

export class Customer {
    /**
     * Creates a new Stripe Customer
     * @param couchDb Couch Instance
     * @param params Customer Object containing registration details
     */
    public static register(couchDb, params: any): Promise<any> {
        return CouchMethods.register(couchDb, params.customer);
    }

    public static login(couchDb, params: any): Promise<any> {
        return CouchMethods.login(couchDb, params.customer);
    }
    public static validateToken(couchDb, params: any): Promise<any> {
        return CouchMethods.validateToken(params.token);
    }
    public static details(couchDb, params: any): Promise<any> {
        return CouchMethods.details(couchDb, params.email);
    }
    public static updateShippingDetails(couchDb, params: any): Promise<any> {
        return CouchMethods.updateShippingDetails(couchDb, params.email, params.shippingDetails);
    }
    public static editShippingDetails(couchDb, params: any): Promise<any> {
        return CouchMethods.editShippingDetails(couchDb, params.email, params.shippingDetails);
    }
    public static changeDefaultAddress(couchDb, params: any): Promise<any> {
        return CouchMethods.changeDefaultAddress(couchDb, params.email, params.index, params.type);
    }

    public static chargeUserCard(couchDb, params: any): Promise<any> {
        return StripeMethods.chargeUserCard(couchDb, params.customerId, params.basketId);
    }
    public static updateCustomerCouch(couchDb, params: any): Promise<any> {
        return CouchMethods.updateCustomer(couchDb, params.email, params.data);
    }

    public static getBasket(couchDb, params: any): Promise<any> {
        return CouchMethods.getBasket(couchDb, params.userId);
    }

    public static changePassword(couchDb, params: any): Promise<any> {
        return CouchMethods.changePassword(couchDb, params.email, params.oldPassword, params.newPassword);
    }

    // -- Stripe Methods

    public static createCustomerStripe(couchDb, params: any): Promise<any> {
        return StripeMethods.createCustomer(params.email);
    }

    public static getCustomerStripe(couchDb, params: any): Promise<any> {
        return StripeMethods.getCustomer(params.email);
    }

    public static updateCustomerStripe(couchDb, params: any): Promise<any> {
        return StripeMethods.updateCustomer(params.id, params.data);
    }

    public static addSourceStripe(couchDb, params: any): Promise<any> {
        return StripeMethods.addSource(params.customerId, params.token);
    }

    public static refund(couchDb, params: any): Promise<object> {
        return StripeMethods.refund(params.charge, params.amount);
    }
    public static addNewCard(couchDb, params: any): Promise<any> {
        return StripeMethods.addNewCard(params.customerId, params.tokenId);
    }

    public static getAllCards(couchDb, params: any): Promise<any> {
        return StripeMethods.getAllCards(params.id);
    }

    public static createCardToken(couchDb, params: any): Promise<any> {
        return StripeMethods.createCardToken(params.card);
    }

    public static deleteCard(couchDb, params: any): Promise<any> {
        return StripeMethods.deleteCard(params.customerId, params.cardId);
    }

    // - Delivery Lookup Methods
    public static returnDeliveryCost(couchDb, params: any): Promise<any> {
        return LookupMethods.returnDeliveryCost(couchDb, params.postcode);
    }

    public static lookupAll(couchDb): Promise<any> {
        return LookupMethods.lookupAll(couchDb);
    }
}
