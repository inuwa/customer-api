const gulp = require('gulp');
const watch = require('gulp-watch');
const path = require('path');
const express = require('gulp-express');
const awsLambdaTypescript = require('aws-lambda-typescript');

awsLambdaTypescript.registerBuildGulpTasks(gulp, __dirname);

gulp.task('lambda:run', () => {
    let debugConfig = require(path.join(__dirname, 'debug-config.js'));

    if (debugConfig && debugConfig.env) {
        Object.keys(debugConfig.env).forEach(function (key) {
            process.env[key] = debugConfig.env[key];
        });
    }

    var options = {};
    options.env = process.env;
    options.env.LAMBDA_PATH = path.join(__dirname, 'index.ts');

    express.run(['local-server.js'], options, false);

    return watch(['./**/*.ts', './**/*.js'], express.run);
});

