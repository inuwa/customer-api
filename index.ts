import { Callback, Context, Handler } from 'aws-lambda';
import { Customer } from './actions/customer';
import * as Jwt from 'jsonwebtoken';
import { default as nanoAsync, ServerScopeAsync } from 'nano-async';
const key = 'MIIEpAIBAAKCAQEAl8xxOa1JKw5Ju4LN6HZsI2hkCJi3r+3aoBXpk0NHvrMZ95VS';
declare var process;

class ProductManagementSystemApi {
    private serverScope = nanoAsync({
        url: 'http://' + process.env.DB_USER + ':' + process.env.DB_PASS + '@' + process.env.DB_URL
    }) as ServerScopeAsync;

    private couchDb;
    private actions: any;

    constructor(
        private event: any,
        private callback: Callback = () => { }
    ) {
        this.actions = {
            login: { db: 'customers', action: Customer.login },
            register: { db: 'customers', action: Customer.register },
            validateToken: { db: 'customers', action: Customer.validateToken },
            details: { db: 'customers', action: Customer.details },
            getBasket: { db: 'pmi', action: Customer.getBasket },
            updateShippingDetails: { db: 'customers', action: Customer.updateShippingDetails },
            editShippingDetails: { db: 'customers', action: Customer.editShippingDetails },
            changeDefaultAddress: { db: 'customers', action: Customer.changeDefaultAddress },
            updateCustomer: { db: 'customers', action: Customer.updateCustomerCouch },
            createCustomerStripe: { db: 'customers', action: Customer.createCustomerStripe },
            updateCustomerStripe: { db: 'customers', action: Customer.updateCustomerStripe },
            getCustomerStripe: { db: 'customers', action: Customer.getCustomerStripe },
            chargeUserCard: { db: null, action: Customer.chargeUserCard },
            changePassword: { db: 'customers', action: Customer.changePassword },
            refund: { db: 'customers', action: Customer.refund },
            lookup: { db: 'delivery_lookup', action: Customer.returnDeliveryCost },
            lookupAll: { db: 'delivery_lookup', action: Customer.lookupAll },
            addNewCard: { db: 'customers', action: Customer.addNewCard },
            getAllCards: { db: 'customers', action: Customer.getAllCards },
            deleteCard: { db: 'customers', action: Customer.deleteCard },
            createCardToken: { db: 'customers', action: Customer.createCardToken }
        };
    }

    public processRequest(): void {
        try {
            const action: string = (this.event && this.event.action) ? this.event.action : null;
            if (this.actions[action]) {
                this.callAction(this.actions[action]);
            } else {
                this.errorResponse('Action not found');
            }
        } catch (error) {
            this.errorResponse('Process Error', error);
        }
    }

    private callAction(action) {
        let valid = true;

        if (action.authRequired) {
            // Validate Token
            if (this.event.token) {
                valid = Jwt.verify(this.event.token, key);
            } else {
                valid = false;
            }
        }

        this.couchDb = (action.db) ? this.serverScope.use(action.db) : this.serverScope;

        if (valid) {
            action.action(this.couchDb, this.event)
                .then((response) => this.successResponse(response))
                .catch((error) => this.errorResponse(error.message, error.details));
        }
    }

    private successResponse(response: any = null): void {
        this.callback(null, response);
    }

    private errorResponse(message: string, response: any = null): void {
        this.callback(null, {
            error: message,
            details: response
        });
    }
}

const handler: Handler = (event: any, context: Context, callback?: Callback): void => {
    let api = new ProductManagementSystemApi(event, callback);
    api.processRequest();
};

export default handler;
