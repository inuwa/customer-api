import { CouchMethods } from './couch-methods';

export class StripeMethods {

    /**
     * Charges the token on the user card
     * @param couch CouchInstance
     * @param userId number
     */
    /* public static chargeUserCard(serverScope: any, id: string) {
        let stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
        return new Promise<any>((resolve, reject) => {
            //  call the basket api
            let pmi = serverScope.use('pmi');
            CouchMethods.getBasketbyId(pmi, id).then((basket) => {
                stripe.charges.create({
                    amount: basket.surcharge || basket.totalAmount,
                    currency: 'gbp',
                    description: 'DIY Kitchen Purchase',
                    source: basket.stripe.token,
                    metadata: {
                        basket: basket._id,
                        revision: basket._rev,
                        userId: basket.userId
                    }
                }, (err, charge) => {
                    if (err) {
                        reject(err);
                    }
                    resolve(charge);
                });
            }).catch((error) => {
                reject(error);
            });
        });
    } */

    public static chargeUserCard(serverScope: any, customerId: string, basketId: string) {
        let stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
        return new Promise<any>((resolve, reject) => {
            //  call the basket api
            let pmi = serverScope.use('pmi');
            CouchMethods.getBasketbyId(pmi, basketId).then((basket) => {
                stripe.charges.create({
                    amount: basket.surcharge || basket.totalAmount,
                    currency: 'gbp',
                    description: 'DIY Kitchen Purchase',
                    customer: customerId,
                    metadata: {
                        basket: basket._id,
                        revision: basket._rev,
                        userId: basket.userId
                    }
                }).then((charge) => {
                    resolve(charge);
                }).catch((err) => reject(err));
            }).catch((error) => {
                reject(error);
            });
        });
    }

    /**
     * Create a new stripe campaign
     * @param {string} email Customer Email
     */
    public static createCustomer(email: string) {
        let stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
        return new Promise<any>((resolve, reject) => {
            return stripe.customers.create({
                email
            }).then((customer) => {
                resolve(customer);
            }).catch((err) => {
                reject(err);
            });
        });
    }

    /**
     * Check if Customer Exists
     * @param customerId of the created customer
     */
    public static getCustomer(email: string) {
        let stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
        return new Promise<any>((resolve, reject) => {
            stripe.customers.list({
                email,
                limit: 1
            }).then((customer) => {
                resolve(customer);
            }).catch((err) => {
                reject(err);
            });
        });
    }

    /**
     * Update the stripe customer
     * @param id customer Id from Stripe database
     * @param metadata Basket ID and other idenetifies
     */
    public static updateCustomer(id: string, data: object) {
        let stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
        return new Promise<any>((resolve, reject) => {
            stripe.customers.update(id, data)
                .then((response) => {
                    resolve(response);
                })
                .catch((err) => reject(err));
        });
    }

    /**
     * Adds the credit card entered by the customer to stripe
     * @param customerId the stripe customer ID
     * @param token the token iD of the stripe customer
     */
    public static addSource(customerId: string, token: string) {
        let stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
        return new Promise<any>((resolve, reject) => {
            stripe.customers.createSource(customerId, { source: token })
                .then((card) => {
                    resolve(card);
                }).catch((err) => {
                    reject(err);
                });
        });
    }

    /**
     * Refunds a customer( charge is the order from the last transaction )
     * @param charge string
     * @param amount number
     */
    public static refund(charge: string, amount: number) {
        let stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
        return new Promise<any>((resolve, reject) => {
            stripe.refunds.create({
                charge,
                amount
            }).then((resp) => {
                resolve(resp);
            }).catch((err) => reject(err));
        });
    }

    /**
     * Adds the credit card details to a customer stripe profile
     * @param id customers stripe id
     * @param cardDetails and object dictionary the card details
     */
    public static addNewCard(customerId: string, tokenId: string): Promise<any> {
        let stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
        return new Promise((resolve, reject) => {
            stripe.customers.createSource(
                customerId,
                { source: tokenId }
            ).then((response) => {
                resolve(response);
            }).catch((err) => reject(err));
        });
    }

    /**
     * Gets all the cards for the user
     * @param id customers stripe id
     */
    public static getAllCards(id: string): Promise<any> {
        let stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
        return new Promise((resolve, reject) => {
            stripe.customers.listCards(
                id
            ).then((cardsObject) => {
                let cards = cardsObject.data;
                resolve(cards);
            }).catch((err) => reject(err));
        });
    }

    /**
     * Deletes a stored card related to a stripe customer
     * @param customerId stripe customerId
     * @param cardId cardId
     */
    public static deleteCard(customerId: string, cardId: string): Promise<any> {
        let stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
        return new Promise((resolve, reject) => {
            stripe.customers.deleteCard(
                customerId,
                cardId
            ).then((response) => {
                if (!response.error) {
                    resolve(response);
                } else {
                    reject(response);
                }
            }).catch((err) => reject(err));
        });
    }

    /**
     * To be used with addNewCardAPI. The token that is created is stored in the customer
     * Stripe Account
     * @param card card object e.g. {
     *   card: {
     *      "number": '4242424242424242',
     *       "exp_month": 12,
     *      "exp_year": 2019,
     *     "cvc": '123'
     *      }
     *   }
     */
    public static createCardToken(card: any): Promise<any> {
        let stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
        return new Promise<any>((resolve, reject) => {
            stripe.tokens.create(card).then((token) => {
                resolve(token);
            }).catch((err) => reject(err));
        });
    }
}
