export class LookupMethods {
    /**
     * Returns the delievery details of the postcode
     * @param couch Couch Db Instance
     * @param postcode The first 2 or Four Letters of the postcode
     */
    public static returnDeliveryCost(couch: any, postcode: string) {
        return new Promise((resolve, reject) => {
            couch.viewAsync('lookup-index', 'lookup-view').then(([body, headers]) => {
                if (body && body.rows.length > 0) {
                    let [document] = body.rows;
                    let [deliveryDetail] = document.value.filter((detail) => detail.postcode === postcode);
                    resolve(deliveryDetail);
                } else {
                    resolve({ error: 'No Records Returned' });
                }
            }).catch((err) => reject(err));
        });
    }

    /**
     * Get All the delivery Details from the lookup table
     * @param couch Couch db instance
     */
    public static lookupAll(couch: any) {
        return new Promise((resolve, reject) => {
            couch.viewAsync('lookup-index', 'lookup-view').then(([body, headers]) => {
                if (body && body.rows.length > 0) {
                    let [document] = body.rows;
                    resolve(document.value);
                } else {
                    resolve({ error: 'No Records Returned' });
                }
            }).catch((err) => reject(err));
        });
    }
}
